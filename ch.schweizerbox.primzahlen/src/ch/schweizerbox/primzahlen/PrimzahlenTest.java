package ch.schweizerbox.primzahlen;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PrimzahlenTest {
	
	
	Primzahlen primzahlen = new Primzahlen(25);
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Pr�ft mit einer separaten Berechnung, ob das Array korrekt abgef�llt wird.
	 */
	@Test
	public final void testSetZahlenbereich() {
		int maximum = 25;
		List actuals = primzahlen.setZahlenbereich();
		List expecteds = new ArrayList<Integer>();
		for (int i = 1; i < maximum; i++) {
			expecteds.add(i);
		}

		assertEquals(expecteds, actuals);
		
		
		
	}

	/**
	 * Pr�ft ob das im Array enthaltene Maximum der Eingabe entspricht.
	 */
	@Test
	public final void testMaxImZahlenbereich() {
		int maximum = 37;
		Primzahlen primzahlen = new Primzahlen(maximum);
		
		List<Integer> list = primzahlen.setZahlenbereich();
		int maxinList = 0;
		for (int i = 0; i < list.size(); i++) {
			if (maxinList < list.get(i)) {
				maxinList = list.get(i);
			}
		}

		int actual = maxinList;
		assertEquals(maximum, actual);
		
	}
	
	
}
